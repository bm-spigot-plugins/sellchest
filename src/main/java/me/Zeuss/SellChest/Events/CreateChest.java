package me.Zeuss.SellChest.Events;

import me.Zeuss.SellChest.Main;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.SignChangeEvent;
import org.bukkit.material.Sign;

import java.util.List;

public class CreateChest implements Listener {

    @EventHandler
    public void onCreate(SignChangeEvent e) {
        if (e.getLine(0).equalsIgnoreCase(Main.getPlugin().yml.getString("settings.create"))) {
            Player p = e.getPlayer();
            if (Main.getPlugin().yml.getBoolean("settings.permission.enabled") && !p.hasPermission("sellchest.create")) {
                e.getBlock().breakNaturally();
                p.sendMessage(ChatColor.translateAlternateColorCodes('&', Main.getPlugin().yml.getString("settings.permission.message")));
                return;
            }
            List<String> lines = Main.getPlugin().yml.getStringList("settings.lines");
            int i = 0;
            e.setLine(0, "");
            e.setLine(1, "");
            e.setLine(2, "");
            e.setLine(3, "");
            for (String l : lines) {
                e.setLine(i++, ChatColor.translateAlternateColorCodes('&', l.replace("%player_name%", p.getName())));
            }
            Block host = e.getBlock().getRelative(((Sign)e.getBlock().getState().getData()).getAttachedFace());
            if (host.getType() != Material.CHEST && host.getType() != Material.TRAPPED_CHEST) {
                e.getBlock().breakNaturally();
                return;
            }
            Main.getPlugin().createChest(p, e.getBlock().getLocation());
        }
    }

}
