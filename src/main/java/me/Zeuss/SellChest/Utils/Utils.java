package me.Zeuss.SellChest.Utils;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

public class Utils {

    public static ItemStack buildItem(Material mat, String data) {
        ItemStack item = new ItemStack(mat);
        if (!data.equalsIgnoreCase("*")) {
            item.setDurability(Short.parseShort(data));
        }
        return item;
    }

    public static Material getMaterial(String value) {
        if (isInt(value)) {
            return Material.getMaterial(Integer.parseInt(value));
        } else {
            return Material.matchMaterial(value);
        }
    }

    public static boolean isInt(String value) {
        try {
            Integer.parseInt(value);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    public static boolean isDouble(String value) {
        try {
            Double.parseDouble(value);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

}
