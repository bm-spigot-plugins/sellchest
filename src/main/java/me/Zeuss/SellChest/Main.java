package me.Zeuss.SellChest;

import java.util.ArrayList;
import java.util.List;

import me.Zeuss.SellChest.Custom.Bonus;
import me.Zeuss.SellChest.Events.CreateChest;
import me.Zeuss.SellChest.Custom.SChest;
import me.Zeuss.SellChest.Custom.SItem;
import net.milkbowl.vault.economy.Economy;
import net.milkbowl.vault.permission.Permission;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.inventory.Inventory;
import org.bukkit.plugin.java.JavaPlugin;

public class Main extends JavaPlugin {

    public static Economy economy = null;
    public static Permission perms = null;
    public List<String> lines = new ArrayList<>();
    public ArrayList<SChest> chests = new ArrayList<>();
    public ArrayList<SItem> itens = new ArrayList<>();
    public ArrayList<Bonus> bonus = new ArrayList<>();
    public FileConfiguration yml;

    @Override
    public void onLoad() {
        saveDefaultConfig();
        yml = getConfig();
        lines = yml.getStringList("locations");
    }

    @Override
    public void onEnable() {
        setupEconomy();
        setupPermissions();
        Bukkit.getPluginManager().registerEvents(new CreateChest(), this);
        loadItens();
        loadBonus();
        loadChests();
        Bukkit.getScheduler().runTaskTimerAsynchronously(this, new Runnable() {
            @Override
            public void run() {
                for (SChest sc : (ArrayList<SChest>) chests.clone()) {
                    sc.validate();
                    if (itens.size() > 0 && sc != null && sc.getChest() != null) {
                        double value = 0;
                        if (sc.isLoaded()) {
                            value = sellItens(sc.getChest().getBlockInventory());
                        } else {
                            sc.reload();
                            if (sc.isLoaded()) {
                                value = sellItens(sc.getChest().getBlockInventory());
                            }
                        }
                        if (value > 0) {
                            if (bonus.size() > 0) {
                                for (Bonus b : bonus) {
                                    value = b.getBonus(sc.getOwnerUUID(), sc.getSignLocation().getWorld(), value);
                                }
                            }
                            economy.depositPlayer(Bukkit.getOfflinePlayer(sc.getOwnerUUID()), value);
                        }
                    }
                }
            }
        }, 20, 20 * yml.getInt("settings.delay"));
    }

    @Override
    public void onDisable() {
    }

    public static Main getPlugin() {
        return Main.getPlugin(Main.class);
    }

    private void loadItens() {
        if (yml != null) {
            Bukkit.getScheduler().runTask(this, new Runnable() {
                @Override
                public void run() {
                    List<String> list = yml.getStringList("settings.itens");
                    for (String l : list) {
                        itens.add(new SItem(l));
                    }
                }
            });
        }
    }

    private void loadChests() {
        if (yml != null) {
            Bukkit.getScheduler().runTask(this, new Runnable() {
                @Override
                public void run() {
                    for (String l : lines) {
                        chests.add(new SChest(l));
                    }
                }
            });
        }
    }

    private void loadBonus() {
        if (yml != null) {
            Bukkit.getScheduler().runTask(this, new Runnable() {
                @Override
                public void run() {
                    List<String> list = yml.getStringList("settings.vantages");
                    for (String l : list) {
                        bonus.add(new Bonus(l));
                    }
                }
            });
        }
    }

    public double sellItens(Inventory inv) {
        double value = 0;
        for (int i = 0; i < inv.getSize(); i++) {
            ItemStack item = inv.getItem(i);
            if (item != null) {
                for (SItem si : itens) {
                    if (si.matchItem(item)) {
                        value += (item.getAmount() * (si.getPrice() / si.getAmount()));
                        inv.setItem(i, null);
                    }
                }
            }
        }
        return value;
    }

    private boolean setupEconomy() {
        RegisteredServiceProvider<Economy> economyProvider = getServer().getServicesManager().getRegistration(Economy.class);
        if (economyProvider != null)
            economy = (Economy)economyProvider.getProvider();
        return (economy != null);
    }

    private boolean setupPermissions() {
        RegisteredServiceProvider<Permission> rsp = getServer().getServicesManager().getRegistration(Permission.class);
        perms = rsp.getProvider();
        return perms != null;
    }

    public void createChest(Player player, Location loc) {
        String schest = "-u:" + player.getUniqueId().toString() + ";-w:" + loc.getWorld().getName().replace(" ", "<_-_>") + ";-x:" + loc.getBlockX() + ";-y:" + loc.getBlockY() + ";-z:" + loc.getBlockZ();
        lines.add(schest);
        chests.add(new SChest(schest));
        getConfig().set("locations", lines);
        saveConfig();
    }

    public void removeChest(String serial) {
        if (lines.contains(serial)) {
            SChest sc = new SChest(serial);
            if (chests.contains(sc)) {
                chests.remove(sc);
            }
            lines.remove(serial);
            getConfig().set("locations", lines);
            saveConfig();
        }
    }

    public void removeChest(Location loc) {
        for (SChest sc : chests) {
            if (sc != null) {
                if (sc.getSignLocation().equals(loc)) {
                    if (chests.contains(sc)) {
                        ArrayList<SChest> clone = ((ArrayList<SChest>) chests.clone());
                        clone.remove(sc);
                        chests = clone;
                    }
                    lines.remove(sc.getSerial());
                    getConfig().set("locations", lines);
                    saveConfig();
                }
            }
        }
    }

}