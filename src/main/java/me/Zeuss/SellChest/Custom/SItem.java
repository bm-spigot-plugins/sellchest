package me.Zeuss.SellChest.Custom;

import me.Zeuss.SellChest.Utils.Utils;
import org.bukkit.inventory.ItemStack;

public class SItem {

    private ItemStack item;
    private int amount;
    private double price;
    private boolean ignore_data;

    public SItem(String serializeItem) {
        String[] split = serializeItem.split(";");
        item = Utils.buildItem(Utils.getMaterial(split[0].contains(":") ? split[0].split(":")[0] : split[0]), split[0].contains(":") ? split[0].split(":")[1] : "*");
        amount = Utils.isInt(split[1]) ? Integer.parseInt(split[1]) : 1;
        price = Utils.isDouble(split[2]) ? Double.parseDouble(split[2]) : 0.0;
        ignore_data = split[0].contains(":") ? split[0].split(":")[1].equalsIgnoreCase("*") : true;
    }

    public ItemStack getItem() {
        return this.item;
    }

    public int getAmount() {
        return this.amount;
    }

    public double getPrice() {
        return this.price;
    }

    public boolean ignoreData() {
        return this.ignore_data;
    }

    public boolean matchItem(ItemStack item) {
        if (ignoreData()) {
            return this.item.getType() == item.getType();
        } else {
            return ((this.item.getType() == item.getType()) && (this.item.getDurability() == item.getDurability()));
        }
    }

}
