package me.Zeuss.SellChest.Custom;

import me.Zeuss.SellChest.Main;
import org.bukkit.Bukkit;
import org.bukkit.World;

import java.util.UUID;

public class Bonus {

    private String node;
    private int percent;

    public Bonus(String serial) {
        node = serial.split(";")[0];
        percent = Integer.parseInt(serial.split(";")[1]);
    }

    public String getPermission() {
        return "sellchest.bonus." + node;
    }

    public double getBonus(UUID uuid, World world, double amount) {
        if (Main.perms.playerHas(world.getName(), Bukkit.getOfflinePlayer(uuid), getPermission())) {
            return (amount + ((amount * percent) / 100));
        }
        return amount;
    }

}
