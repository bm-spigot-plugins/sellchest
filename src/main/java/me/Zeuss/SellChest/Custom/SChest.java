package me.Zeuss.SellChest.Custom;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import me.Zeuss.SellChest.Main;
import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.block.Chest;
import org.bukkit.entity.Player;
import org.bukkit.material.Sign;

public class SChest {

    private UUID owner;
    private Location location;
    private Chest chest;
    private String serial;

    public SChest(String serializedChest) {
        this.serial = serializedChest;
        String name = null;
        int x = 0, y = 0, z = 0;
        for (String v : serializedChest.split(";")) {
            if (v.split(":")[0].equalsIgnoreCase("-u")) {
                this.owner = UUID.fromString(v.split(":")[1]);
            } else if (v.split(":")[0].equalsIgnoreCase("-w")) {
                name = v.split(":")[1].replace("<_-_>", " ");
            } else if (v.split(":")[0].equalsIgnoreCase("-x")) {
                x = Integer.parseInt(v.split(":")[1]);
            } else if (v.split(":")[0].equalsIgnoreCase("-y")) {
                y = Integer.parseInt(v.split(":")[1]);
            } else if (v.split(":")[0].equalsIgnoreCase("-z")) {
                z = Integer.parseInt(v.split(":")[1]);
            }
        }
        this.location = new Location(Bukkit.getWorld(name), x, y, z);

        Chunk c = this.location.getChunk();
        if (c.isLoaded()) {
            this.chest = buildChest();
        }
    }

    public void reload() {
        Chunk c = this.location.getChunk();
        if (c.isLoaded()) {
            this.chest = buildChest();
        } else {
            this.chest = null;
        }
    }

    public boolean isLoaded() {
        return this.location.getChunk().isLoaded();
    }

    public Chest getChest() {
        return this.chest;
    }

    public UUID getOwnerUUID() {
        return this.owner;
    }

    public Player getPlayer() {
        return Bukkit.getPlayer(getOwnerUUID());
    }

    public Location getSignLocation() {
        return this.location;
    }

    public String getSerial() {
        return this.serial;
    }

    private Chest buildChest() {
        try {
            Block host = this.location.getBlock().getRelative(((Sign)this.location.getBlock().getState().getData()).getAttachedFace());
            return (org.bukkit.block.Chest) host.getLocation().getBlock().getState();
        } catch (Exception e) {
            return null;
        }

    }

    public void validate() {
        Block b = this.location.getBlock();
        if (!(Arrays.asList(new Material[] { Material.SIGN, Material.SIGN_POST, Material.WALL_SIGN }).contains(b.getType()))) {
            Main.getPlugin().removeChest(location);
            return;
        } else {
            try {
                org.bukkit.block.Sign s = ((org.bukkit.block.Sign) this.location.getBlock().getState());
                List<String> lines = Main.getPlugin().getConfig().getStringList("settings.lines");
                int i = 0;
                for (String l : lines) {
                    if (!s.getLine(i).equalsIgnoreCase(ChatColor.translateAlternateColorCodes('&', l)) && !l.contains("%player_name%")) {
                        location.getBlock().breakNaturally();
                        Main.getPlugin().removeChest(location);
                        return;
                    }
                    i++;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

}